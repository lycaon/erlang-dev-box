
node "erldb" {

  include taskjp
  include erlang
  include nginx

  package { "git-core":
    ensure => installed,
  }
}

